cmake_minimum_required (VERSION 3.13.5)

project (customlib)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

find_package(liblua REQUIRED)

add_subdirectory (customlib)
