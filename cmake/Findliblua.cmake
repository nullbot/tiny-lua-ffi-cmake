cmake_minimum_required (VERSION 3.13.5)

find_library(liblua_LIBRARY NAMES lua51
    PATHS customlib/lib/32bit)

if (liblua_LIBRARY)
   set(liblua_FOUND TRUE)
endif ()

if (liblua_FOUND)   
   message(STATUS "Found liblua: ${liblua_LIBRARY}")     
elseif (liblua_FIND_REQUIRED)
      message(FATAL_ERROR "Could not find liblua. Please install liblua and liblua-devel packages.")
endif ()
