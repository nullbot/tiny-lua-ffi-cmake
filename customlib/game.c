#include "lua.h"
#include "lauxlib.h"
#include "game.h"
#include "version.h"

#if defined _WIN32 || defined _WIN64 || defined __CYGWIN__
#define DLL_PUBLIC __declspec(dllexport)
#else
	#define DLL_PUBLIC
#endif

char* get_version_string()
{
    return LIB_VERSION;
}

static int get_lib_version(lua_State* L)
{
    lua_pushstring(L, get_version_string());
    return 1;
}

DLL_PUBLIC int luaopen_customlib(lua_State* L)
{
    static const luaL_reg Map[] = {
        {"get_lib_version", get_lib_version},
        {NULL,NULL}
    };
    luaL_register(L, "customlib", Map);
    return 1;
}
